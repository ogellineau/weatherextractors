import json
import re
import requests
from bs4 import BeautifulSoup

try:
	# We are attempting to run via cli within mFisheries application
	from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
	# We are attempting to run via cli outside of application
	from WeatherSourceExtractor import WeatherSourceExtractor


def findword(word,array):
		for data in array:
			if word in data.lower():
				return data


def findwordList(word,array):
		save = ""
		for data in array:
			if word in data.lower():
				save = data
		return save


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(self,"http://slumet.gov.lc/category/uncategorized/")

	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			},
			"wind speed": {
				"type": "numerical",
				"unit": "mph"
			},
			"waves": {
				"type": "numerical",
				"unit": "feet"
			}
		}
	
	def extract(self):
		items = []
		forecast = []
		self.readings = {}

		# Make Request from the Website
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")

		# Strategy for caching request while developing
		# filename = "input.html"
		# with open(filename, "r") as fp:
		# 	fp.write(str(soup))
		# 	content = fp.read()
		# 	fp.close()
		# soup = BeautifulSoup(content, "html.parser")

		# Capture all the paragraphs within the content section
		data = soup.find_all("div", {"class": "entry-content"})
		for row in data:
			for list in row.find_all('p'):
				if len(list.text) > 1:
					items.append(list.text.encode('utf-8').replace("\xc2\xa0", " ").replace("\xe2\x80\x89", " "))

		# Process the items to capture useful information
		for paragraph in items:
			try:
				# Extract Present Weather summary
				if len(paragraph.upper().split("PRESENT WEATHER")) > 1:
					if 'present weather' not in self.readings:
						self.readings['present weather'] = ""
					self.readings['present weather'] += paragraph.lower().capitalize() + " "
				# Extract Present Temperature and store as temperature
				elif len(paragraph.upper().split("PRESENT TEMPERATURE")) > 1:
					if len(paragraph.upper().split("C OR")) > 1:
						self.readings['temperature'] = re.sub("[^0-9]", "", paragraph.split("C OR")[0])
				# Extract Current Wind Measured at HEWANORRA
				elif len(paragraph.upper().split("WIND AT HEWANORRA")) > 1:
					self.readings['current wind'] = paragraph.lower().capitalize()
				# Extract Forecast wind and use value for wind threshold-based measurement
				elif len(paragraph.upper().split("WINDS WILL ")) > 1:
					if len(paragraph.upper().split("MPH OR")) > 1:
						self.readings['wind speed'] = re.sub("[^0-9]", "", paragraph.split("MPH OR")[0])
				# Extract Weather forecast
				elif len(paragraph.upper().split('WEATHER:')) > 1:
					self.readings['weather forecast'] = paragraph.lower().capitalize()
				# Extract High and Low tides
				elif len(paragraph.upper().split('TIDES FOR')) > 1:
					split = paragraph.lower().split(":")
					if len(split) > 1:
						self.readings[self.clean_text(split[0])] = ':'.join(split[1:]).capitalize()
				# Extract sea conditions and waves
				elif len(paragraph.upper().split("SEAS:")) > 1:
					split = paragraph.lower().split(":")
					if len(split) > 1:
						self.readings['seas'] = split[1].lower().capitalize()
						if len(paragraph.upper().split("TO")) > 2:
							sec = paragraph.upper().split("TO")[2]
							if len(sec.upper().split("FEET")) > 1:
								self.readings['waves'] = re.sub("[^0-9]", "", sec.split("FEET")[0])
				# Extract Date
				elif len(paragraph.upper().split("DATE:")) > 1:
					self.readings['date'] = paragraph.upper().split("DATE:")[1].lower().replace("\n", " ")
			except:
				print("Error processing data: {0}".format(paragraph))

		sun = findword('sunset',items).split(" ")
		count =0
		for i in sun:
			if "AM" in i:
				self.readings['sunrise'] = sun[count-1].strip() + " AM"
			elif "PM" in i:
				self.readings['sunset'] = sun[count-1].strip() + " PM"
			count += 1

		try:
			if 'temperature' not in self.readings:
				temperature = findword('temperature',items).split("C OR")[0]
				self.readings['temperature'] = re.sub("[^0-9]", "", temperature)
			if 'current wind' not in self.readings:
				list = findwordList('wind',items).lower().split("\n")
				for i in range(len(list)):
					if "wind" in list[i]:
						self.readings['current wind'] = list[i].capitalize()
			if 'seas' not in self.readings:
				self.readings['seas'] = findword("seas:",items).split("SEAS: ")[1].lower().capitalize()
		except:
			print("Unable to retrieve data for st lucia")

		for key in self.readings.keys():
			self.readings[key] = self.clean_text(self.readings[key])

		return self.readings


	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
	country = Extractor()
	country.extract()
	# print(country.get_reading_types())
	print(country.toJSON())
	# base_url = "http://slumet.gov.lc/category/uncategorized/page"
	# print("[")
	# for page in range(1, 10):
	# 	url = "{0}/{1}".format(base_url, page)
	# 	country.extractor_url = url
	# 	country.extract()
	# 	# print(country.get_reading_types())
	#
	# 	print(country.toJSON())
	# 	print(",")
	# print("]")