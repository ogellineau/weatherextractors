import json
import re
import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor



# Accepts array of paragraphs and returns which paragraph text is found in
def findword(word,array):
    for data in array:
        if word in data.lower():
            return data


def findwordList(word,array):
    save = ""
    for data in array:
        if word in data.lower():
            save = data
    return save


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "http://www.antiguamet.com/Antigua_Met_files/SKB_FCast.html",

        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "wind": {
                "type": "numerical",
                "unit": "mph"
            },
            "waves": {
                "type": "numerical",
                "unit": "feet"
            }
        }

    def extract(self):
        self.readings = {}
        r = requests.get( "http://www.antiguamet.com/Antigua_Met_files/SKB_FCast.html")
        soup = BeautifulSoup(r.content,"html.parser")
        data = soup.find_all("p")
        tdata = soup.find_all("tr")
        for t in tdata:
            if "high" in t.text.lower():
                high = t.text.replace(" ", "")
            if "low" in t.text.lower():
                low = t.text.replace(" ", "")

        high = re.sub("[^0-9]", "",high.split("C")[0])
        self.readings['high Temp'] = high

        low = re.sub("[^0-9]", "",low.split("C")[0])
        self.readings['low Temp'] = low

        for x in data:
            if "synopsis:" in x.text.lower():
                self.readings['synopsis'] = x.text

            if "seas:" in x.text.lower():
                self.readings['seas'] = x.text

            if "winds:" in x.text.lower():
                self.readings['winds'] = x.text

            if "sunrise tomorrow" in x.text.lower():
                self.readings['sunrise'] = x.text

            if "sunset" in x.text.lower():
                self.readings['sunset'] = x.text

            if "pressure:" in x.text.lower():
                self.readings['pressure'] = x.text

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)

if __name__ == "__main__":
	country = Extractor()
	country.extract()
	print(country.get_reading_types())
	print(country.toJSON())

