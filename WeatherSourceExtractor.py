from abc import ABCMeta, abstractmethod

class WeatherSourceExtractor(object):
    __metaclass__ = ABCMeta

    def __init__(self, extract=""):
        self.extractor_url = extract
        self.readings = {}

    @abstractmethod
    def get_extractor_url(self):
        pass

    @abstractmethod
    def toJSON(self):
        pass

    @abstractmethod
    def get_reading_types(self):
        pass

    @abstractmethod
    def extract(self):
        pass

    def save(self, sourceid=0):
        data = self.extract()
        print("Attempting to save data: " + str(data))

    def clean_text(self, data):
        try:
            data = data.replace('\n', ' ').replace('\r', ' ')
        except Exception, e:
            print(e)

        try:
            data = data.replace(u'\xa0', " ")
        except Exception, e:
            print(e)
        # try:
        #     data = data.replace('e280a6', "-")
        # except Exception, e:
        #     print(e)


        return data
    
    def findword(self, word,array):
        """
        Accepts array of paragraphs and returns which paragraph text is found in
        """
        items = []
        for data in array:
            if word in data.lower():
                items.append(data)
        return items
    
    def findwordList(self, word, array):
        save = ""
        for data in array:
            if word in data.lower():
                save = data
        return save

    def wordsize(self,stuff, word):
        string = ""
        pos = stuff.find(word) + len(word)
        while not (stuff[pos].isupper() and stuff[pos - 1].islower()):
            string += stuff[pos]
            pos += 1
        return string

    def comparison(self, newalert, oldalert):
        return set(newalert) == set(json.loads(oldalert))

    def weather_alert(self, sourceid):
        pass