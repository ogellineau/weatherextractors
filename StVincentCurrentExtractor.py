import json
import re
import requests
from bs4 import BeautifulSoup

try:
	# We are attempting to run via cli within mFisheries application
	from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
	# We are attempting to run via cli outside of application
	from WeatherSourceExtractor import WeatherSourceExtractor

# St.VincentCurrentForecastExtractor
class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://www.barbadosweather.org/Overseers/Wx_Data/getWxForecastData.php?country=Saint%20Vincent"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"wind": {
				"type": "text",
				"unit": "km/h"
			},
			"seas": {
				"type": "text",
				"unit": "m"
			}
		}
	
	def extract(self):
		weather = []
		self.readings = {}
		items = []
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content,"html.parser")
		forecast = []
		for row in soup.find_all("tr"):
			for i in row.find_all('td'):
				items.append(i.text)

		self.readings['synopsis'] = self.findword("synopsis",items)[0].split(": ")[1]
		self.readings['wind'] = self.findword("wind",items)[0].split(": ")[1]
		self.readings['seas'] = self.findword("sea",items)[0].split(": ")[1]	
		for row in soup.find_all("div",{"style":"text-align:left;"}):
			weather.append(row.text.strip().split(": ")[1])
		
		self.readings['weather'] = weather[0]
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	country.extract()
	print(country.get_reading_types())
	print(country.toJSON())