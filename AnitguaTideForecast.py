import json
import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "https://www.tide-forecast.com/locations/Saint-Georges-Harbour-Grenada/tides/latest"
        )

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "tides": {
                "type": "text",
                "unit": ""
            },
            "sunmoon": {
                "type": "text",
                "unit": ""
            }
        }

    def toJSON(self):
        return json.dumps(self.readings, indent=4)

    def extract(self):
        self.readings = {}
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")
        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, "r") as fp:
        # fp.write(str(soup))
        # content = fp.read()
        # fp.close()
        # soup = BeautifulSoup(content, "html.parser")

        weather_data = []
        temp_date = None
        # Process the table rows as individual records
        for table_row in soup.select("table tr"):
            # Process the table a  column at a time (cells stores all the columns)
            head = table_row.find('th')
            cells = table_row.findAll('td')
            # If we have a date in this column then we update the fields
            if head:
                temp_date = head.text.strip()
                print(temp_date)

            date = temp_date

            if len(cells) <= 5:
                time = cells[0].text.strip()
                time_zone = cells[1].text.strip()
                level_metric = cells[2].text.strip()
                level_imperial = cells[3].text.strip().replace("(", "").replace(")", "")
                event = cells[4].text.strip()
            else:
                temp_date = cells[0].text.strip()
                date = temp_date
                time = cells[1].text.strip()
                time_zone = cells[2].text.strip()
                level_metric = cells[3].text.strip()
                level_imperial = cells[4].text.strip().replace("(", "").replace(")", "")
                event = cells[5].text.strip()

            weather = {
                'date': date,
                'time': time,
                'time_zone': time_zone,
                'level_metric': level_metric,
                'level_imperial': level_imperial,
                'event': event
            }
            weather_data.append(weather)

        self.readings['sunmoon'] = list(filter(
            lambda x: x['event'] == "Sunrise" or x['event'] == "Sunset" or x['event'] == "Moonrise" or x[
                'event'] == "Moonset", weather_data))
        self.readings['tides'] = list(
            filter(lambda x: x['event'] == "High Tide" or x['event'] == "Low Tide", weather_data))


if __name__ == "__main__":
    country = Extractor()
    country.extract()
    # print(country.get_reading_types())
    # print(country.toJSON())
    with open('output.json', "w") as fp:
        fp.write(country.toJSON())
        fp.close()
