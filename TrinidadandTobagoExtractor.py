import json
import re
import requests
from bs4 import BeautifulSoup

try:
    # We are attempting to run via cli within mFisheries application
    from mfisheries.modules.fewer.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except:
    # We are attempting to run via cli outside of application
    from WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
    def __init__(self):
        WeatherSourceExtractor.__init__(
            self,
            "http://metservice.gov.tt/forecast_api2.php"
        )
        self.debug = False

    def get_poster_url(self):
        return self.post_url

    def get_extractor_url(self):
        return self.extractor_url

    def get_reading_types(self):
        return {
            "waves": {
                "type": "numerical",
                "unit": "m"
            }

        }

    def extract(self):
        headings = []
        details = []
        self.readings = {}
        count = 0
        r = requests.get(self.extractor_url)
        soup = BeautifulSoup(r.content, "html.parser")

        # Strategy for caching request while developing
        # filename = "input.html"
        # with open(filename, "r") as fp:
        # 	# fp.write(str(soup))
        # 	content = fp.read()
        # 	fp.close()
        # soup = BeautifulSoup(content, "html.parser")

        spans = []
        # for row in soup.find_all("span"):
        # 	print(row.text)

		stuff = filter(None, soup.text.split("\n"))
		temp = filter(None, soup.text.split("\n"))
		data = " ".join(stuff)
		# print(data)

        self.readings['waves'] = self.wordsize(data, "Waves: ")
        self.readings['seas'] = self.wordsize(data, "Seas: ")
        sun = self.findwordList('sunset', stuff).split(" ")
        count = 0
        for t in temp:
            if "tonight:" in t.lower():
                r = t.split("room")
                self.readings['synopsis'] = r[1].replace("\r", "") + temp[count + 1].replace("\r", "")

            if "max. temp." in t.lower():
                self.readings["Forecasted Max Temp, Piarco"] = temp[count + 1]
                self.readings["Forecasted Max Temp, Crown Point"] = temp[count + 1]

            count = count + 1

        count = 0
        for i in sun:
            if "sunset:" in i.lower():
                self.readings['sunset'] = sun[count + 1]

			if "AM" in i or "am" in i:
				self.readings['sunrise'] = sun[count - 1].strip()
			count = count + 1

        return self.readings

    def toJSON(self):
        return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor

if __name__ == "__main__":
    country = Extractor()
    country.extract()
    print(country.get_reading_types())
    print(country.toJSON())
