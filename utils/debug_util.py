import logging
DEBUG = True
DEBUG_TYPE = "log"  #log
# DEBUG_TYPE = "print"

def display_debug(msg, type="debug", debug_type=None, module_name=__name__):

	if debug_type is None:
		debug_type = DEBUG_TYPE
	if DEBUG:
		if debug_type is "print":
			print("{0}: {1}".format(type, msg))
		else:
			log = logging.getLogger(module_name)
			if type is "debug":
				log.debug(msg)
			elif type is "error":
				log.error(msg)
			elif type is "info":
				log.info(msg)
			else:
				log.debug(msg)


def get_ini_path(use_development=True):
	import os
	filename = "development.ini" if use_development else "production.ini"
	path = "./"
	max_depth = 3
	num_tries = 0
	while num_tries < max_depth:
		display_debug("Attempting to find {0} at {1}".format(filename, path))
		file_path = "{0}/{1}".format(path, filename)
		if os.path.isfile(file_path):
			return file_path
		path = "{0}../".format(path)
		num_tries = num_tries + 1
	return None

